-- SELECT i.Id,
--        i.Timestamp,
--        i.CreatedBy,
--        i.ModifiedBy,
--        i.DeletedBy,
--        i.CheckpointEquipmentId,
--        i.InspectionDueDate,
--        i.LastInspectionOrganization,
--        i.CreatedOn,
--        i.ModifiedOn,
--        i.DeletedOn,
--        i.InspectionDate,
--        i.InspectionType,
--        i.CalibrationType,
--        i.CalibrationApplicationSubmitted,
--        i.ExaminedBy,
--        i.RelatedInspectionId,
--        i.DocumentId,
--        EID.Filename
SELECT distinct CONCAT(I.DocumentId, '.pdf') Name, EID.Content
FROM ELECTRA_Checkpoints_Equipments E
         JOIN ELECTRA_Inspections I ON I.CheckpointEquipmentId = E.Id
         JOIN ELECTRA_Inspections_Documents EID on I.DocumentId = EID.Id
WHERE E.IsDeactivated = 0
  AND E.State in (4, 25)
  AND I.InspectionDueDate > '2021-03-31'
  AND I.LastInspectionOrganization like '%dekra%'
  AND EID.Content is not null
  AND I.InspectionType = 2
  AND EID.DeletedOn is null
  AND E.DeletedOn is null
  AND I.DeletedOn is null

