﻿namespace SQLBlobToFile
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string OutputFolder { get; set; }
        public string SqlFilesFolder { get; set; }
        public string FileNameOrPattern { get; set; }
        public int SqlCommandTimeoutInSeconds { get; set; }
    }
}