﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace SQLBlobToFile
{
    internal class Program
    {
        public const string AppSettings = "appsettings.json";
        private static IConfiguration BuildConfiguration()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile(AppSettings, true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            var config = builder.Build();
            Console.WriteLine($"Built Configuration for Environment {env}");
            return config;
        }

        private static async Task Main()
        {
            Console.WriteLine($"Started");
            var config = BuildConfiguration();

            try
            {
                var worker = new Worker(config);
                await worker.DoWorkAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("Press any button to exit...");
            Console.ReadKey();
        }
    }
}
