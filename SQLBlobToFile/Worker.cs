﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Humanizer;
using Humanizer.Bytes;
using Microsoft.Extensions.Configuration;
// ReSharper disable PossibleLossOfFraction

namespace SQLBlobToFile
{
    public class Worker
    {
        public IConfiguration Configuration { get; }
        private AppSettings Settings { get; set; }

        public Worker(IConfiguration configuration)
        {
            this.Configuration = configuration;
            this.Configure();
        }

        private void Configure()
        {
            this.Settings = this.Configuration.GetSection("App").Get<AppSettings>();
            this.CheckStartConditions();
        }

        private void CheckStartConditions()
        {
            Console.WriteLine("Starting Configuration checks");
            if (string.IsNullOrWhiteSpace(this.Settings.OutputFolder) || !Directory.Exists(this.Settings.OutputFolder))
                throw new ArgumentException(
                    $"Wrong {nameof(this.Settings.OutputFolder)} in {Program.AppSettings}: {this.Settings.OutputFolder}");

            if (string.IsNullOrWhiteSpace(this.Settings.SqlFilesFolder) || !Directory.Exists(this.Settings.SqlFilesFolder))
                throw new ArgumentException(
                    $"Wrong {nameof(this.Settings.SqlFilesFolder)} in {Program.AppSettings}: {this.Settings.SqlFilesFolder}");

            if (string.IsNullOrWhiteSpace(this.Settings.ConnectionString))
                throw new ArgumentException(
                    $"Provide a {nameof(this.Settings.ConnectionString)} in {Program.AppSettings}");

            var (result, exception) = this.CanConnect();
            if (result == false)
                throw new ArgumentException($"Correct the {nameof(this.Settings.ConnectionString)}", exception);
            Console.WriteLine("Configuration checks successful");
        }

        private (bool result, Exception exception) CanConnect()
        {
            try
            {
                Console.WriteLine("\tTrying to open a SQL connection");
                using (var connection = new SqlConnection(this.Settings.ConnectionString))
                {
                    connection.Open();
                    connection.Close();
                }

                Console.WriteLine("\tConnection successful");
            }
            catch (Exception e)
            {
                return (false, e);
            }

            return (true, null);
        }

        public async Task DoWorkAsync()
        {
            Console.WriteLine($"Searching {this.Settings.FileNameOrPattern} File(s) in {this.Settings.SqlFilesFolder}");
            var files = Directory.GetFiles(this.Settings.SqlFilesFolder, this.Settings.FileNameOrPattern);
            Console.WriteLine($"Found {files.Length} SQL Files");
            foreach (var pathToFile in files)
            {
                var file = new FileInfo(pathToFile);
                Console.WriteLine($"Found {file.Name} with a size of {file.Length.Bytes().ToString("#.##")}");
                var text = await File.ReadAllTextAsync(pathToFile);
                try
                {
                    using (var connection = new SqlConnection(this.Settings.ConnectionString))
                    {
                        var sw = Stopwatch.StartNew();
                        await connection.OpenAsync();
                        Console.WriteLine($"{sw.Elapsed}: Opened Connection");
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandText = text;
                            command.CommandTimeout = this.Settings.SqlCommandTimeoutInSeconds;
                            command.CommandType = CommandType.Text;

                            using (var reader = await command.ExecuteReaderAsync())
                            {
                                Console.WriteLine($"{sw.Elapsed}: SQL Command returns Rows: {reader.HasRows}");
                                if(!reader.HasRows) return;

                                var columns = await reader.GetColumnSchemaAsync();
                                Console.WriteLine($"{sw.Elapsed}: Found {columns.Count} Columns");
                                if (CheckColumns(columns)) return;

                                var dataColumnIndex = GetColumnIndex(columns, typeof(byte[]));
                                var nameColumnIndex = GetColumnIndex(columns, typeof(string));

                                Console.WriteLine($"{sw.Elapsed}: found name column at index {nameColumnIndex} and data column at {dataColumnIndex}");

                                await this.ReadDataAndWriteFiles(reader, nameColumnIndex, dataColumnIndex, sw);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        private static bool CheckColumns(ICollection columns)
        {
            if (columns.Count <= 2) return false;
            Console.WriteLine("Exiting: Sql Statement should only return 2 Columns, one for content and one for a filename");
            return true;

        }

        private static int GetColumnIndex(IList<DbColumn> columns, Type type)
        {
            foreach (var dbColumn in columns)
            {
                if (dbColumn.DataType == type)
                    return columns.IndexOf(dbColumn);
            }

            throw new ArgumentException($"Could not determine column of type {type.Name} in SQL Statement");
        }

        private async Task ReadDataAndWriteFiles(DbDataReader reader, int nameColumnIndex, int dataColumnIndex, Stopwatch sw)
        {
            var counter = 0;
            long fileSizeCounter = 0;
            while (await reader.ReadAsync())
            {
                counter++;
                var fileName = reader.GetString(nameColumnIndex);
                var contentStream = reader.GetStream(dataColumnIndex);
                using (var exportFile = File.Create(Path.Combine(this.Settings.OutputFolder, fileName)))
                {
                    fileSizeCounter += contentStream.Length;
                    await contentStream.CopyToAsync(exportFile);
                    exportFile.Close();
                }

                if (counter % 50 == 0)
                    Console.WriteLine(
                        GetStatusMessage(sw, counter, fileSizeCounter));
            }

            Console.WriteLine(GetStatusMessage(sw, counter, fileSizeCounter));
            Console.WriteLine("Finished!");
        }

        private static string GetStatusMessage(Stopwatch sw, int counter, long fileSizeCounter)
        {
            return $"{sw.Elapsed}: Exported {counter} Documents with a Size of {fileSizeCounter.Bytes().ToString("#.##")}" +
                   $" ({ByteSize.FromBytes(fileSizeCounter / sw.ElapsedMilliseconds).Per(TimeSpan.FromMilliseconds(1)).Humanize("#.##")})";
        }
    }
}